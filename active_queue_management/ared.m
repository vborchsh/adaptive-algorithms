
%
% Adaptive Random Early Detection (ARED) algorithm implementation
%
% Based on Adaptive RED: An Algorithm for Increasing the Robustness 
% of RED�s Active Queue Management
% SallyFloyd, RamakrishnaGummadi, andScottShenker
% http://www.icsi.berkeley.edu/pubs/networking/adaptivered01.pdf
%

clear all;
close all;
clc;

%% Parameters
pSTEP       = 2;         % Steps for update input variable (packet speed)
pN          = 2048;      % Iterations
pFIX_TARGET = 1;         % Fixed target value or adaptive (w_q)
pK          = 0.4;       % Target coefficient (0.4..0.6)
pMIN_TH     = 0.2;       % Threshold minimum
pMAX_TH     = 4*pMIN_TH; % Threshold maximum

%% Create variables
avg = 0;
beta = 0.9;
alfa = 0.01; % 0.01..max_p/4
max_p = ones(1, pN+1) * 0.5;

min_th = pMIN_TH*100;

% Main loop
for i = 1:pN
    max_th = round(100 - (100 * max_p(i)));
    
    if (max_th < min_th)
        max_th = min_th
    end
    
    if (mod(i, pSTEP) == 0)
        avg = randi([min_th max_th],1,1)/100;
    end
    avg_arr(i) = avg;
    C = avg;
    
    alfa = min(0.01, 0.25*max_p(i));
    
    if pFIX_TARGET
        target = pMIN_TH + pK*(pMAX_TH - pMIN_TH);
    else
        target = 1 - exp(-1/C); % w_q
    end
     
    if (avg > target && max_p(i) <= 0.5)
        max_p(i+1) = max_p(i) + alfa;
    elseif (avg < target && max_p(i) >= 0.01)
        max_p(i+1) = max_p(i) * beta;
    else
        max_p(i+1) = max_p(i);
    end
end

tarr = zeros(1,pN);
tarr(1,:) = target;

figure
hold on
plot(smoothdata(avg_arr, 'movmean', 128)),
plot(smoothdata(max_p, 'movmean', 128)),
plot(tarr);